# K8s Operator Sample

a sample k8s operator project

```
# create app (webapp guestbook) crd
kubectl create -f config/crd/bases/webapp.example.com_guestbooks.yaml

# create the new "guestbook" k8s object
kubectl create -f config/samples/webapp_v1_guestbook.yaml

# query the guestbook objects
kubectl get guestbooks

# create redis crd
kubectl create -f config/crd/bases/webapp.example.com_redis.yaml

# create the new "redis" k8s object
kubectl create -f config/samples/webapp_v1_redis.yaml

# query the redis objects
kubectl get redis

# run the operator implementation
make run
```

```
this sample project was scaffolded by https://github.com/kubernetes-sigs/kubebuilder
and developed using GO. You need to install GO on your workbench to enjoy this sample project.
```
